cmake_minimum_required(VERSION 3.5)

include(extras/tools.cmake)
tools_get_git_last_commit("${CMAKE_SOURCE_DIR}" _var_LAST_COMMIT)

project(Cisson
        VERSION "0.0.1.${_var_LAST_COMMIT}"
        DESCRIPTION "Cisson - JSON library in C language."
        HOMEPAGE_URL "https://gitlab.com/Meithal/cisson"
        LANGUAGES C)

set(CMAKE_C_STANDARD 23)

message(STATUS "${CMAKE_PROJECT_NAME} version ${CMAKE_PROJECT_VERSION}")
message(STATUS "Running on cmake ${CMAKE_VERSION}")
message(STATUS "Compiler : ${CMAKE_C_COMPILER_ID} ${CMAKE_C_COMPILER_VERSION}" )
message(STATUS "Build tool : ${CMAKE_BUILD_TOOL}" )
message(STATUS "Cmake command : ${CMAKE_COMMAND}" )

#add_custom_command(OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/cisson.h
#    DEPENDS
#        ${CMAKE_CURRENT_SOURCE_DIR}/extras/json.h
#        ${CMAKE_CURRENT_SOURCE_DIR}/extras/json.c
#    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_SOURCE_DIR}/extras/concat.cmake
#    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
#    COMMENT "Running extras/concat.cmake"
#    VERBATIM
#)

add_custom_target(run_concat_script
    DEPENDS
        extras/json.h
        extras/json.c
        extras/concat.cmake
    BYPRODUCTS cisson.h
    COMMAND ${CMAKE_COMMAND} -P extras/concat.cmake
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Running extras/concat.cmake to produce a single-header version of the library."
    VERBATIM
)

if (MSVC)
    add_compile_options(/W4)
    add_compile_definitions(_CRT_SECURE_NO_WARNINGS)
else()
    add_compile_options(-pedantic)
    add_compile_options(-Wall -Wextra -Wpedantic)
    # some stdlib funtions wont work if including a libC
    add_compile_definitions(_POSIX_C_SOURCE=200809L)
endif()

set(JSON_FILES extras/json.h extras/json.c)

add_compile_definitions(LAST_COMMIT_COUNT="${_var_LAST_COMMIT}")
add_compile_definitions(CMAKE_GENERATOR="${CMAKE_C_COMPILER}")

add_library(xjson SHARED ${JSON_FILES})
add_library(sjson STATIC ${JSON_FILES})

target_include_directories(sjson PUBLIC extras)
target_include_directories(xjson PUBLIC extras)

if(CMAKE_PROJECT_NAME STREQUAL Cisson)
    include(CTest)
endif()

if(BUILD_TESTING)
    foreach(Kind BAKED STATIC SHARED)
        foreach(WantLibc "" libc_)
            foreach (Concat "" concat_)

                set(ExecutableName "testing_auto_${Kind}_${WantLibc}${Concat}exe")

                add_executable("${ExecutableName}" ./extras/tests.c)

                set(JSON_FILES
                        extras/json.h
                        extras/json.c)
                if(Concat)
                    target_compile_definitions(${ExecutableName} PUBLIC TEST_SINGLE_HEADER)
                    add_dependencies(${ExecutableName} run_concat_script)
                    set(JSON_FILES "${CMAKE_SOURCE_DIR}/cisson.h")
                endif()

                target_sources(${ExecutableName} PRIVATE ${JSON_FILES})

                if(NOT Kind STREQUAL BAKED)
                    set(LibName "auto_${Kind}_${WantLibc}${Concat}")
                    add_library("${LibName}" "${Kind}" "")

                    target_sources(${LibName} PRIVATE ${JSON_FILES})

                    if(Concat)
                        add_dependencies(${LibName} run_concat_script)
                        target_compile_definitions(${LibName} PRIVATE JSN_IMPLEMENTATION)
                        target_sources(${LibName} PRIVATE extras/single_header.c)
                    endif()

                    set_target_properties("${LibName}" PROPERTIES LINKER_LANGUAGE C)
                    target_link_libraries("${ExecutableName}" PRIVATE "${LibName}")

                    get_target_property(sources "${LibName}" SOURCES)
                    message(STATUS "Library ${LibName} sources ${sources}")
                else ()
                    target_sources(${ExecutableName} PRIVATE ${JSON_FILES})
                    target_compile_definitions(${ExecutableName} PUBLIC JSN_IMPLEMENTATION)
                endif()

                if (WantLibc)
                    target_compile_definitions(${ExecutableName} PUBLIC "WANT_LIBC")
                endif ()

                set(TestName "auto_${Kind}_${WantLibc}${Concat}test")
                add_test(NAME ${TestName} COMMAND ${ExecutableName})

            endforeach ()
        endforeach()
    endforeach()
endif()

if(IS_DIRECTORY extras/yyjson_benchmark)
    add_subdirectory(extras/yyjson_benchmark)
endif ()

set(CMAKE_C_STANDARD_LIBRARIES "")
