# Json recipes

**The library is still in alpha stage
and the API may change at any moment.
Look for the tests on how to use the lib.**

***

To turn the following C object into JSON

```C
struct foo = {
  .foo = "bar",
  .array = {1, 2, 3},
  .question = true
};
```

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"

#include <stdio.h>

int main(void) {
    struct json_tree json_tree = {0};
    
    /* every token we push will be bound to the current root */
    START_AND_PUSH_TOKEN(&json_tree, "{");
    PUSH_ROOT(&json_tree);
    /* "PUSH_ROOT" will change the current root to the previously
     * pushed token so every following
     * token will be bound to the object token */
    START_AND_PUSH_TOKEN(&json_tree, "\"foo\"");
    PUSH_ROOT(&json_tree);
    /* next token will be bound to the key */
    START_AND_PUSH_TOKEN(&json_tree, "\"bar\"");
    CLOSE_ROOT(&json_tree);
    /* now the root is { again */
    START_AND_PUSH_TOKEN(&json_tree, "\"array\"");
    PUSH_ROOT(&json_tree);
    START_AND_PUSH_TOKEN(&json_tree, "[");
    PUSH_ROOT(&json_tree);
    START_AND_PUSH_TOKEN(&json_tree, "1");
    START_AND_PUSH_TOKEN(&json_tree, "2");
    START_AND_PUSH_TOKEN(&json_tree, "3");
    CLOSE_ROOT(&json_tree); /* the array */
    CLOSE_ROOT(&json_tree); /* the object key the array is bound to */
    START_AND_PUSH_TOKEN(&json_tree, "\"question\"");
    PUSH_ROOT(&json_tree);
    START_AND_PUSH_TOKEN(&json_tree, "true");
    
    puts(to_string(&json_tree)); /* {"foo":"bar","array":[1,2,4],"question":true} */
}
```

We notice that `START_AND_PUSH_TOKEN` takes a tree, a token kind, and the token itself 
as arguments. We can notice that the kind of the token can be guesses by the first character
of it. For example a string starts with « " », an object with "{" and so on.

We can also notice that every time we push a "{", a "[", or a string whose root is an object,
we push a new root. We can't really guess when we want to close a root though.

We can see the macro only takes strings as arguments (this library only works with strings).

There is also no need to close the final roots, so you can resume the building of a JSON tree
later.

All this logic is implemented by the macro function `push_token`, so the previous code
can be rewritten as

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"

#include <stdio.h>

int main(void) {
    struct json_tree json_tree = {0};
    
    push_token(&state, "{");
    push_token(&state, "\"foo\"");
    push_token(&state, ":");
    push_token(&state, "\"bar\"");
    push_token(&state, ">");
    push_token(&state, "\"array\"");
    push_token(&state, ":");
    push_token(&state, "[");
    push_token(&state, "1");
    push_token(&state, "2");
    push_token(&state, "4");
    push_token(&state, ">");
    push_token(&state, ">");
    push_token(&state, "\"question\"");
    push_token(&state, ":");
    push_token(&state, "true");
    puts(to_string(&state));
    
    puts(to_string(&json_tree)); /* {"foo":"bar","array":[1,2,4],"question":true} */
}
```
A ">" translates to a `CLOSE_ROOT()`.

We now have repeated calls to a function that always take the same argument, and a string.
`stream_tokens` will do those repeated calls for you. 
It takes a separator (which shouldn't exist inside your json data)
and your json data as a single string.

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"
#include <stdio.h>

int main(void) {
    struct json_tree json_tree = {0};

    stream_tokens(
      &json_tree, 
      '~',
      "{~\"foo\"~:~\"bar\"~>~\"array\"~:~[~1~2~4~>>~\"question\"~:~true~>~\"\"~:~null"
    );
    
    puts(to_string(&json_tree)); /* {"foo":"bar","array":[1,2,4],"question":true} */
}
```

But this isn't much different from using `rjson`.

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"
#include <stdio.h>

int main(void) {
    struct json_tree json_tree = {0};

    rjson(
      "[1,2,3,4]"
      &json_tree, 
    );
    
    puts(to_string(&json_tree)); /* [1, 2, 3, 4] */
}
```

One big difference between `stream_tokens` and `rjson` is that `rjson` 
will enforce correct JSON grammar and won't accept incomplete or 
incorrect JSON.

Finally, you can use the standard C library to have numbers converted 
to strings.

```C
#define JSN_IMPLEMENTATION
#include "cisson.h"
#include <stdio.h>

int main(void) {
    struct json_tree json_tree = {0};
    char buffer[100] = {0};

    sprintf(buffer, "[1,2,%d,%d,%s]", 4, 5, "\"foo\"");
    rjson(
      buffer,
      &json_tree 
    );
    
    puts(to_string(&json_tree)); /* [1, 2, 4, 5, "foo"] */
}
```

***

This is how to create two separate JSON documents.

```C

#define JSN_IMPLEMENTATION
#include <cisson.h>

int main() {
    /** 
     * If you zero initialize a tree it will use a pool
     * of static memory pre-allocated by the library, 
     * if you want to have more than one tree in memory
     * this won't work, so this is how you can do it.
     * **/
    
    struct json_tree tree1 = { 0 };
    struct json_tree tree2 = { 0 };
    
    struct token stack1[40]; // 40 tokens
    struct token stack2[40];
    unsigned char pool1[400]; // every token text will be stacked here
    unsigned char pool2[400];
    
    start_state(&tree1, stack1, pool1);
    start_state(&tree2, stack2, pool2);
    /* start_state() can also be used to quickly, 
     * reset an existing json_tree to reuse it.
     * If you only need the reset feature and use 
     * static memory, start_state(&tree, NULL, NULL);
     * or memset(&tree, 0, sizeof tree) works too. */
    
    rjson("[1, 2, 3, [4, 5, 6], [7, 8, 9]", &tree1);
    rjson("{\"foo\": 1}", &tree2);
    
    insert_token(&tree2, "\"array\"", query(&tree2, ""));
    /* tree2 is now {"bar":1,"array"} (not valid JSON). */
    move_token(&tree1, query(&tree1, "/1"), query(&tree1, "/4"));
    /*  tree1 is now [1, 3, [4, 5, 6], [7, 8, 9, 2]. */
    inject(
        to_string_pointer( &tree1, query(&tree1, "/3")),
        &tree2, query(&tree2, "/array/<")
    );
    
    /**
     * to_string_pointer() extracts JSON from anywhere
     * within a json_tree.
     * 
     * inject() injects JSON text anywhere inside a json_tree.
     * 
     * tree2 is now {"bar":1,"array":[7,8,9,2]}
     * 
     * The < bit targets the "array" node itself rather 
     * than its descendant.
     * 
     * query(..., "/array) targets [7,8,9,2]
     * query(..., "/array/<) targets "array"
     **/
    
    delete_token(query(&tree1, "/3"));
    /* tree1 is now [1, 3, [4, 5, 6]] */
    
    rename_string(&tree2, query(&tree2, "/foo/<"), "bar");
    /**
     * rename_string() works like
     * 
     * insert_token(&tree2, "\"bar\"", query(&tree2, ""));
     * move_token(&tree2, query(&tree2, "/foo"), query(&tree2, "/bar/<");
     * delete(&tree2, query(&tree2, "/foo/<");
     */
    
    puts(to_string_compact(&tree1)); /* [1,3,[4,5,6]] */
    puts(to_string_compact(&tree2)); /* {"bar":1,"array":[7,8,9,2]} */
    
    /* print every value of /array */
    struct token *root, *cur;
    for(root = query(&tree2, "/array"),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, cur));
    }
    /* 7 8 9 2 */
    
    /* print every root object value */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, next_child(&tree2, cur, NULL)));
    }
    /* 1 [7,8,9,2] */
    
    /* print every root object key */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, cur));
    }
    /* "bar":1 "array":[7,8,9,2] */
    
    /* Json printing functions will print every 
     * token bound to the root we provide.
     * To print the keys without their children, we have
     * to peek at the raw json_tree object and extract the strings 
     * they point at, this isn't supported but if you really
     * need that feature you can use this recipe. */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(
            " %.*s",
            *(string_size_type_raw*)
            ((char*)cur->address - sizeof string_size_type),
            cur->address
        );
    }
    /* "bar" "array" */

}

```

## API

Exported functions are
* `start_state_` if you need to reuse a tree without `memset()`
it or make it use memory you allocated yourself.
* `query_` to find a token inside a tree, it is based on json pointer
but has some extension.
* `next_child_` to parse siblings of any host capable token.
* `rjson_` to check json text for validity and store it inside
an ast.
* `inject_` to check json text for validity and inject it inside
an existing ast.
* `to_string_` to convert a tree to json text.
* `start_string` internal use by parsers, but have exported 
visibility to be used by macro helpers. Ideally you will never
have to use it.
* `push_string` idem.
* `close_root` idem.
* `push_root` idem.
* `push_token_kind` idem.
* `delete_token_` deletes a token and all its descendents.
* `move_token_` moves a token and all its descendents somewhere else
inside the tree.
* `rename_string_` makes a token point to a given string.
* `insert_token_` understand "smart" syntax, ultimately calls
`push_token_kind`
* `stream_tokens_` works with `insert_token_`.
  
As far as exported functions go, non suffixed ones should never
be used, and the ones that have a _ suffix should be used with
caution as their signature may change at any time.

Macro helpers below give full control on how the ast is built.

* tok_len
* START_STRING
* PUSH_STRING
* CLOSE_ROOT
* PUSH_ROOT
* PUSH_TOKEN
* INSERT_TOKEN
* PUSH_STRING_TOKEN
* INSERT_STRING_TOKEN
* START_AND_PUSH_TOKEN
* START_AND_INSERT_TOKEN

To make it safer to use the library the following
macro helpers have a more stable api, 
and have a stronger guarantee that their parameter 
signature won't change, but their name may change.

They also ignore the prefixing mechanism, so their name
will not change depending on where the library is used, 
and since they are macro functions you can #undef them 
and redefine them under another name if you 
find them clashing with an existing name.

These macro functions are 

*(no explanation means they call
their suffixed functions verbatim)*

* `next_child`
* `start_state`
* `rjson` will use `strlen` to know how many characters 
of json text to read.
* `rjson_len` will read limited portion of json text and won't
stop at null bytes (in which case an error will be reported
since they're not allowed).
* `inject`
* `to_string` writes the complete json tree inside pre-allocated
memory and return a pointer to it. Ultimately you may
have to write your own to_string_* to have better control on how you
want your json text to come out.
* `to_string_compact` writes tree in compact form.
* `to_string_pointer` writes a subsection of tree.
* `to_string_sink` writes inside arbitrary memory.
* `delete_token`
* `move_token`
* `rename_string`
* `insert_token` will insert a token on any host inside the tree.
* `push_token` will insert a token on whatever the current host 
of the tree is.
* `query`
* `stream_tokens` stream tokens at the end of the tree.
* `stream_into` stream tokens anywhere inside the tree.

Basic use is
>`rjson()` reads JSON and creates a json_tree to represent it.
```c
struct json_tree json_tree = { 0 };
rjson("[10, 20, 30, 40]", &json_tree);
```
>`to_string()` writes JSON inside a static buffer whose size
can be customized at compilation time and by default is
`0x2000` bytes long. `to_string_sink()` lets you declare
where you want the JSON to be written.
```c
puts(to_string(&json_tree)); /* [10, 20, 30, 40] */ 
char sink[0x100];
puts(to_string_sink(&json_tree, query(&json_tree, "/2"), sink, 0x100)); /* 30 */
```
>`to_string_pointer()`extracts JSON from a json_tree, from the
place pointed by `query()`.
```c
puts(to_string_pointer(&json_tree, query(&json_tree, "/3"))); /* 40 */
puts(to_string_pointer(&json_tree, query(&json_tree, ""))); /* [10, 20, {"foo": 12}, 30, 40] */
```
>`next_child()` browses JSON compound values in an iterator-like
fashion.
```c
struct token *root, *cur;
for(root = query(&json_tree, ""),
    cur = next_child(&json_tree.tokens, root, NULL);
    cur != NULL;
    cur = next_child(&json_tree.tokens, root, cur)) {
    printf(" %s", to_string_pointer(&state2, cur));
}
/* 10 20 {"foo":12} 30 40 */
```
