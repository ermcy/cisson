#ifdef TEST_SINGLE_HEADER
#include "../cisson.h"
#else
#include "../extras/json.h"
#endif

#include <stdio.h>
#include <assert.h>
#include <string.h>

#if (!(defined(_WIN32) || defined(_WIN64)) \
|| defined(__CYGWIN__) \
|| defined(__MINGW32__) \
|| defined(__MINGW64__))
#define LONGLONG long long
#define NO_MSVC
#include <time.h>
#else
#include <Windows.h>
#endif

#ifndef NO_MSVC
static LONGLONG
start_profiler() {
    LARGE_INTEGER frequency;

    QueryPerformanceFrequency(&frequency);

    return frequency.QuadPart;
}

static LONGLONG
start_timer() {
    LARGE_INTEGER starting_time;
    QueryPerformanceCounter(&starting_time);

    return starting_time.QuadPart;
}

static LONGLONG
elapsed(LONGLONG start, LONGLONG frequency) {
    LARGE_INTEGER ending_time;
    QueryPerformanceCounter(&ending_time);
    LARGE_INTEGER elapsed_microseconds = {
            .QuadPart=ending_time.QuadPart - start
    };

    elapsed_microseconds.QuadPart *= 1000000;
    elapsed_microseconds.QuadPart /= frequency;

    return elapsed_microseconds.QuadPart;
}
#else
static LONGLONG
start_profiler(void) {
    return 0;
}

static LONGLONG
start_timer(void) {
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);
    return start.tv_sec * 1000000000 + start.tv_nsec;
}

static LONGLONG
elapsed(LONGLONG start, LONGLONG frequency) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec) * 1000000000 + end.tv_nsec - start
           + ((frequency != 0) & 0); // shut down unused variable warning, clamp it to 0
}

#endif

static struct {
    char const * str;
    char const * ref;
} valid_json[] = {
    {"true", "true"},
    {"false", "false"},
    {"null", "null"},
    {"true  ", "true"},
    {"  \t  true  ", "true"},
    {"  \t  true", "true"},
    {" true", "true"},
    {"10", "10"},
    {"-0", "-0"},
    {"1", "1"},
    {"123", "123"},
    {"1203.4", "1203.4"},
    {"123.0", "123.0"},
    {"123.05", "123.05"},
    {"123.0500", "123.0500"},
    {"123.0500000", "123.0500000"},
    {"0.12", "0.12"},
    {"-0.12", "-0.12"},
    {"-0.12e1", "-0.12e1"},
    {"-0e3", "-0e3"},
    {"-0e-3", "-0e-3"},
    {"123.0e12", "123.0e12"},
    {"123.0343300e12", "123.0343300e12"},
    {"123.000E000", "123.000E000"},
    {"123.0010E0010", "123.0010E0010"},
    {"12e0", "12e0"},
    {"12e000000012", "12e000000012"},
    {"12e000000000", "12e000000000"},
    {"12e00100", "12e00100"},
    {"12e+0", "12e+0"},
    {"12e-0", "12e-0"},
    {"12e1", "12e1"},
    {"-12e1", "-12e1"},
    {"12e01", "12e01"},
    {"12e+01", "12e+01"},
    {"-12e+01", "-12e+01"},
    {"12e-01", "12e-01"},
    {"-12e-01", "-12e-01"},
    {"-12.34e+25", "-12.34e+25"},
    {"-12.34E+25", "-12.34E+25"},
    {"-12.34E-25", "-12.34E-25"},
    {"-12.34e25", "-12.34e25"},
    {"\"foo\"", "\"foo\""},
    {"\"foo\\\\/\\b\\f\\n\\r\\t\"", "\"foo\\\\/\\b\\f\\n\\r\\t\""},
    {"\"foo\\u1234\"", "\"foo\\u1234\""},
    {" \" a random string\"", "\" a random string\""},
    {"[1, 2, \"foo\", [1, 2], 4  ]  ", "[1,2,\"foo\",[1,2],4]"},
    {"[1, 2, 3]", "[1,2,3]"},
    {"[1, {\"foo\" : 12, \"foo\":12}, { \"foo\":\"42\" } ,2, 3]", "[1,{\"foo\":12,\"foo\":12},{\"foo\":\"42\"},2,3]"},
    {"[1, \"2\", 3, \"\", \"\", \"foo\"]", "[1,\"2\",3,\"\",\"\",\"foo\"]"},
    {"[1 , 2 , 3 ]", "[1,2,3]"},
    {"[1, 2, 3  ]", "[1,2,3]"},
    {"[1, 2, 3  ]  ", "[1,2,3]"},
    {" \" à random string é with lower block characters.\"", "\" à random string é with lower block characters.\""},
    {" \" \xCD\xBF random unicode string\"", "___"},
    {" \" a random string with \\u0000 correctly encoded null byte.\"", "\" a random string with \\u0000 correctly encoded null byte.\""},
    {"{\r\n\t  \"foo\": \"bar\"}", "{\"foo\":\"bar\"}"},
    {"{\"foo\": 1, \"bar\": \"foo\"}", "{\"foo\":1,\"bar\":\"foo\"}"},
    {"{\"foo\": 0}", "{\"foo\":0}"},
    {"{\"foo\": 0 }", "{\"foo\":0}"},
    {"{\"foo\": -0}", "{\"foo\":-0}"},
    {"{\"foo\": -0 }", "{\"foo\":-0}"},
    {"{\"foo\": 12e0}", "{\"foo\":12e0}"},
    {"{\"foo\"  : 12e0}", "{\"foo\":12e0}"},
    {"{\"foo\"  : 12e0  }", "{\"foo\":12e0}"},
    {"{\"foo\": 12E0}", "{\"foo\":12E0}"},
    {"{\"foo\": 12E12}", "{\"foo\":12E12}"},
    {"{\"foo\": 12E12 }", "{\"foo\":12E12}"},
    {"{\"foo\": 12.0E12}", "{\"foo\":12.0E12}"},
    {"{\"foo\": 12.0E12 }", "{\"foo\":12.0E12}"},

    {"[1, 2, \"foo\", true]", "[1,2,\"foo\",true]"},

    {"{"
        "    \"Image\": {"
            "\"Width\":  800,"
            "\"Height\": 600,"
            "\"Title\":  \"View from 15th Floor\","
            "\"Thumbnail\":"
                "{\"Url\": \"http://www.example.com/image/481989943\","
                "              \"Height\": 125,"
                "\"Width\":  \"100\""
            "},"
            "\"IDs\": [116, 943, 234, 38793]"
        "}"
    "}", "___"},

    {"["                           /* 49 */
        "{"
            " \"precision\": \"zip\","
            "\"Latitude\":  37.7668,"
            "                    \"Longitude\": -122.3959,"
            "                    \"Address\"\t:   \"\",\t"
            "\"City\"  :      \"SAN FRANCISCO\","
            "\"State\":     [\"CA\", {\"foo\": [\"CA\", \"OK\"]}]  ,"
            "\"Zip\":       \"94107\","
            "\"Country\":\"US\""
        "},"
        "{"
            "\"precision\": \"zip\","
            "\"Latitude\":  37.371991,"
            "\"Longitude\": -122.026020,"
            "\"Address\":   \"\","
            "\"City\":      \"SUNNYVALE\","
            "\"State\":     \"CA\","
            "\"Zip\":       \"94085\","
            "\"Country\":   \"US\""
        "}"
    "]", "___"},
    {"[[[0]]]", "[[[0]]]"},
    {"[[[0] ] ]", "[[[0]]]"},
    {"[ [ [0]]]", "[[[0]]]"},
    {" [ [ [0] ] ] \t", "[[[0]]]"},
    {"[[[1, 3, [3, 5], 7]], 3]", "[[[1,3,[3,5],7]],3]"},
    {"{\"foo\": 1, \"foo\": 1, \"foo\": 2, \"foo\": 1}", "{\"foo\":1,\"foo\":1,\"foo\":2,\"foo\":1}"},
    {"null", "null"},
    {"null  ", "null"},
    {"  null  ", "null"},
    {"\"tést\"", "\"tést\""},
    {"\"test 漫 \"", "\"test 漫 \""},
//    "\"\xFF\"", (char[]){'\"', '\xFF', '\xFD', '\"', '\0'},  /* invalid utf is replaced by \\uFFFD */          /* 59 */ This can be useful as an option
//    "\"\xAF\"", (char[]){'\"', '\xFF', '\xFD', '\"', '\0'},
    {"\"\xFF\"", (char[]){'\"', '\xFF', '\"', '\0'}},
    {"\"\xAF\"", (char[]){'\"', '\xAF', '\"', '\0'}},
    {(char[]){'\xEF', '\xBB',  '\xBF', '4', '2', '\0'}, "42"},
    {"12310000000000000033432.2E324342423423423224234234", "12310000000000000033432.2E324342423423423224234234"},
    {"\"1very long string very long string very long string "
    "2very long string very long string very long string "
    "3very long string very long string very long string "
    "4very long string very long string very long string "
    "5very long string very long string very long string "
    "6very long string very long string very long string "
    "7very long string very long string very long string "
    "8very long string very long string very long string "
    "9very long string very long string very long string "
    "10very long string very long string very long string "
    "11very long string very long string very long string "
    "12very long string very long string very long string "
    "13very long string very long string very long string "
    "14very long string very long string very long string "
    "15very long string very long string very long string "
    "16very long string very long string very long string "
    "17very long string very long string very long string "
    "18very long string very long string very long string "
    "19very long string very long string very long string "
    "20very long string very long string very long string "
    "21very long string very long string very long string "
    "22very long string very long string very long string "
    "23very long string very long string very long string "
    "24very long string very long string very long string "
    "25very long string very long string very long string "
    "26very long string very long string very long string "
    "27very long string very long string very long string "
    "28very long string very long string very long string \"", "___"},
    {" []", "[]"},
    {" [  ]", "[]"},
    {" [ ]", "[]"},
    {"[\"\\\"\\\\/\\b\\f\\n\\r\\t\"]", "[\"\\\"\\\\/\\b\\f\\n\\r\\t\"]"},
    {"{}", "{}"},
    {"{ }", "{}"},
    {"{  }", "{}"},
    {"[[]]", "[[]]"},
    {"[[ ] ]", "[[]]"},
    {"[ []]", "[[]]"},
    {"[[[]]]", "[[[]]]"},
    {"[[[{}]]]", "[[[{}]]]"},
    {"[[[{ } ] ] ]", "[[[{}]]]"},
    {"[ [ [ { } ] ] ]", "[[[{}]]]"},
    {"[1,{}]", "[1,{}]"},
    {"[{},1]", "[{},1]"},
    {"[{},{}]", "[{},{}]"},
    {"[[{}],{}]", "[[{}],{}]"},
    {"[[[{}],{}]]", "[[[{}],{}]]"},
    {"{\"\": []}", "{\"\":[]}"},
    {"{\"\": [2]}", "{\"\":[2]}"},
    {"[{\"\": []}]", "[{\"\":[]}]"},
    {"[[[{\"\": [2],\"\": [[]]}],{}]]", "[[[{\"\":[2],\"\":[[]]}],{}]]"},
    {"[[[{\"\": [],\"\": [[]]}],{}]]", "[[[{\"\":[],\"\":[[]]}],{}]]"},
    {"[[]   ]", "[[]]"},
};

/* shouldn't be parsed as valid json */
static struct{
    char const * str;
    struct {
        enum json_state st;
        int cur;
    } dbg;
} bogus_json[] = {
    {"", {JSS_DOC_START, 0}},
    {" ", {JSS_WS_BEFORE_VALUE, 1}},
    {" \xEFtrue", {JSS_WS_BEFORE_VALUE, 1}},
    {" \xEF\xBB\xBFtrue", {JSS_WS_BEFORE_VALUE, 1}},
    {"true\xEF\xBB\xBF", {JSS_TRUE_E, 4}},
    {"[\xEF\xBB\xBF]", {JSS_ARRAY_START, 1}},
    {"<!-- comment -->", {JSS_DOC_START, 0}},
    {"//comment", {JSS_DOC_START, 0}},
    {"\r\n\t ", {JSS_WS_BEFORE_VALUE, 4}},
    {"\r\n\f \"\r and \n are valid but \f is invalid whitespace\"", {JSS_WS_BEFORE_VALUE, 2}},
    {"NULL", {JSS_DOC_START, 0}},
    {"nUll", {JSS_NULL_N, 1}},
    {"nul", {JSS_NULL_L, 3}},
    {"foo", {JSS_FALSE_F, 1}},
    {"\1\2\3true\4\5\6", {JSS_DOC_START, 0}},
    {"true false", {JSS_WS_AFTER_VALUE_ROOT, 5}},
    {"true'", {JSS_TRUE_E, 4}},
    {"true //comment", {JSS_WS_AFTER_VALUE_ROOT, 5}},
    {"true <!--comment -->", {JSS_WS_AFTER_VALUE_ROOT, 5}},
    {"true, false", {JSS_TRUE_E, 4}},
    {"true,", {JSS_TRUE_E, 4}},
    {"\"forget closing quote", {JSS_IN_STRING, 21}},
    {"\"too many quotes\"\"", {JSS_STRING_CLOSE, 17}},
    {"'wrong quote'", {JSS_DOC_START, 0}},
    {"\"incomplete \\u122 unicode\"", {JSS_ESCAPE_UNICODE_4, 17}},
    {"\"characters between 00 and 1F must use the unicode codepoint notation, \\u0000, not \1, \2, \0 .\"", {JSS_IN_STRING, 83}},
    {"\"true and\" false", {JSS_WS_AFTER_VALUE_ROOT, 11}},
    {"0123", {JSS_NUMBER_ZERO, 1}},
    {"00", {JSS_NUMBER_ZERO, 1}},
    {"123 345", {JSS_WS_AFTER_VALUE_ROOT, 4}},
    {"+0", {JSS_DOC_START, 0}},
    {"+00", {JSS_DOC_START, 0}},
    {"+1", {JSS_DOC_START, 0}},
    {"+01", {JSS_DOC_START, 0}},
    {"+12", {JSS_DOC_START, 0}},
    {"-", {JSS_NUMBER_MINUS, 1}},
    {"- ", {JSS_NUMBER_MINUS, 1}},
    {"--", {JSS_NUMBER_MINUS, 1}},
    {"12 .12", {JSS_WS_AFTER_VALUE_ROOT, 3}},
    {"12.&", {JSS_NUMBER_COMMA, 3}},
    {"12. 12", {JSS_NUMBER_COMMA, 3}},
    {"12 e", {JSS_WS_AFTER_VALUE_ROOT, 3}},
    {"12e 0", {JSS_NUMBER_EXPO_e, 3}},
    {"12e+ 0", {JSS_NUMBER_EXPO_PLUS, 4}},
    {"12e +0", {JSS_NUMBER_EXPO_e, 3}},
    {"12.4 e+0", {JSS_WS_AFTER_VALUE_ROOT, 5}},
    {"12.4 e +0", {JSS_WS_AFTER_VALUE_ROOT, 5}},
    {"--12", {JSS_NUMBER_MINUS, 1}},
    {"-f12", {JSS_NUMBER_MINUS, 1}},
    {"-1-2", {JSS_NUMBER, 2}},
    {"-12-.", {JSS_NUMBER, 3}},
    {"-12.-0", {JSS_NUMBER_COMMA, 4}},
    {"-12.34e+25.2", {JSS_NUMBER_EXPO_NUMBER, 10}},
    {"-12e12-2", {JSS_NUMBER_EXPO_NUMBER, 6}},
    {"-12e++12-2", {JSS_NUMBER_EXPO_PLUS, 5}},
    {"-12e+-122", {JSS_NUMBER_EXPO_PLUS, 5}},
    {"-12e--12-2", {JSS_NUMBER_EXPO_MINUS, 5}},
    {".12", {JSS_DOC_START, 0}},
    {"0.", {JSS_NUMBER_COMMA, 2}},
    {"0. ", {JSS_NUMBER_COMMA, 2}},
    {"-0.", {JSS_NUMBER_COMMA, 3}},
    {"0.e", {JSS_NUMBER_COMMA, 2}},
    {"-0.E", {JSS_NUMBER_COMMA, 3}},
    {"0.t", {JSS_NUMBER_COMMA, 2}},
    {"-0.~", {JSS_NUMBER_COMMA, 3}},
    {"0e", {JSS_NUMBER_EXPO_e, 2}},
    {"-0e", {JSS_NUMBER_EXPO_e, 3}},
    {"12.", {JSS_NUMBER_COMMA, 3}},
    {"12]", {JSS_NUMBER, 2}},
    {"12}", {JSS_NUMBER, 2}},
    {"12,", {JSS_NUMBER, 2}},
    {"12..", {JSS_NUMBER_COMMA, 3}},
    {"12..001", {JSS_NUMBER_COMMA, 3}},
    {"12.001.", {JSS_NUMBER_COMMA_NUMBER, 6}},
    {"12.3.", {JSS_NUMBER_COMMA_NUMBER, 4}},
    {"12.e10", {JSS_NUMBER_COMMA, 3}},
    {"NaN", {JSS_DOC_START, 0}},
    {"nan", {JSS_NULL_N, 1}},
    {"Infinity", {JSS_DOC_START, 0}},
    {"0x12EF", {JSS_NUMBER_ZERO, 1}},
    {"123E", {JSS_NUMBER_EXPO_E, 4}},
    {"123e", {JSS_NUMBER_EXPO_e, 4}},
    {"123.4e", {JSS_NUMBER_EXPO_e, 6}},
    {"123.4E", {JSS_NUMBER_EXPO_E, 6}},
    {"0e+", {JSS_NUMBER_EXPO_PLUS, 3}},
    {"0e-", {JSS_NUMBER_EXPO_MINUS, 3}},
    {"123.4e23e", {JSS_NUMBER_EXPO_NUMBER, 8}},
    {"123.4E23e", {JSS_NUMBER_EXPO_NUMBER, 8}},
    {"123.4e23.", {JSS_NUMBER_EXPO_NUMBER, 8}},
    {"123.4e23,", {JSS_NUMBER_EXPO_NUMBER, 8}},
    {"12,3.4e23,", {JSS_NUMBER, 2}},
    {"123.4e,23,", {JSS_NUMBER_EXPO_e, 6}},
    {"123.4e2,3,", {JSS_NUMBER_EXPO_NUMBER, 7}},
    {"{true: 1}", {JSS_OBJECT_START, 1}},
    {"{false: 1}", {JSS_OBJECT_START, 1}},
    {"[false, 1] true", {JSS_WS_AFTER_VALUE_ROOT, 11}},
    {"[false, 1], true", {JSS_ARRAY_CLOSE, 10}},
    {"{\"false\": 1}, true", {JSS_OBJECT_CLOSE, 12}},
    {"{\"false\": 1, \"true\"}", {JSS_STRING_CLOSE, 19}},
    {"{null: 1}", {JSS_OBJECT_START, 1}},
    {"{2: 1}", {JSS_OBJECT_START, 1}},
    {"{[true]: 1}", {JSS_OBJECT_START, 1}},
    {"{{}: 1}", {JSS_OBJECT_START, 1}},
    {"{[]: 1}", {JSS_OBJECT_START, 1}},
    {"{\"test\"}", {JSS_STRING_CLOSE, 7}},
    {"{\"test\":}", {JSS_OBJECT_COLON, 8}},
    {"{\"test\":1}}", {JSS_OBJECT_CLOSE, 10}},
    {"[[[0]]]]", {JSS_ARRAY_CLOSE, 7}},
    {"[[[0]}]", {JSS_ARRAY_CLOSE, 5}},
    {"[0,[]", {JSS_ARRAY_CLOSE, 5}},
    {"[0,[],", {JSS_ARRAY_COMMA, 6}},
    {"\"no shortcuts \a, \v, \' \047 \"", {JSS_IN_STRING, 14}},
    {"\"no shortcuts \\a, \\v, \\' \\047 \"", {JSS_STRING_ESCAPE_START, 15}},
    {"[1, 2, 3,]", {JSS_ARRAY_COMMA, 9}},
    {"[1, 2,, 3,]", {JSS_ARRAY_COMMA, 6}},
    {"[1, 2, , 3,]", {JSS_ARRAY_WS_EXPECT_VALUE, 7}},
    {"{,}", {JSS_OBJECT_START, 1}},
    {"{\"a\",2}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\",\"a\":2}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\",\"a\"}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\",\"a\":}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\"{\"w\":2}}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\":2,}", {JSS_OBJECT_COMMA, 7}},
    {"{\"a\" : 2 ,}", {JSS_OBJECT_COMMA, 10}},
    {"{\"a\" : 2 , }", {JSS_OBJECT_WS_AFTER_COMMA, 11}},
    {"{ \"a\" : 2 , }", {JSS_OBJECT_WS_AFTER_COMMA, 12}},
    {"{\"a\".: 2 ,}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\"e: 2 ,}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\"E: 2 ,}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\"-: 2 ,}", {JSS_STRING_CLOSE, 4}},
    {"{\"a\" : ,2 }", {JSS_OBJECT_COLON, 7}},
    {"{\"a\" : 2, \"b\" }", {JSS_WS_AFTER_VALUE_OBJECT_KEY, 14}},
    {"{\"a\" : 2,, \"b\" }", {JSS_OBJECT_COMMA, 9}},
    {"{\"a\" : 2 , , \"b\" }", {JSS_OBJECT_WS_AFTER_COMMA, 11}},
    {"{\"a\" : 2 ,\"b\", }", {JSS_STRING_CLOSE, 13}},
    {"{\"a\" : 2 ,\"b\":, }", {JSS_OBJECT_COLON, 14}},
    {"{\"a\" : 2 ,\"b\": 3, }", {JSS_OBJECT_WS_AFTER_COMMA, 18}},
    {"\"invalid escape \\x \"", {JSS_STRING_ESCAPE_START, 17}},
    {"\"invalid escape \\4 \"", {JSS_STRING_ESCAPE_START, 17}},
    {"\"incomplete escape \\u123 \"", {JSS_ESCAPE_UNICODE_4, 24}},
    {"\"incomplete escape \\u  \"", {JSS_ESCAPE_UNICODE_1, 21}},
    {"\"incomplete escape \\u-  \"", {JSS_ESCAPE_UNICODE_1, 21}},
    {"\"incomplete escape \\u1-  \"", {JSS_ESCAPE_UNICODE_2, 22}},
    {"\"incomplete escape \\u12-  \"", {JSS_ESCAPE_UNICODE_3, 23}},
    {"\"incomplete escape \\u123-  \"", {JSS_ESCAPE_UNICODE_4, 24}},
    {"\"incomplete escape \\u1234\1  \"", {JSS_ESCAPE_UNICODE_LAST, 25}},
    {(char[]){' ', '\xEF', '\xBB', '\xBF', '4', '2', '\0'}, {JSS_WS_BEFORE_VALUE, 1}},
    // fixme: should be JSS_BOM_2
    {(char[]){'\xEF', '\xBB', '4', '2', '\0'}, {JSS_BOM_3, 2}},
    {(char[]){'\xEF', '4', '2', '\0'}, {JSS_BOM_2, 1}},
    {(char[]){'\xBB', '4', '2', '\0'}, {JSS_DOC_START, 0}},
    {"[\"	\"]", {JSS_STRING_START, 2}},
    {"[\"new\n\nline\"]", {JSS_IN_STRING, 5}},
    {"[\"\\\"\\\\\\/\b\f\n\r\t\"]", {JSS_IN_STRING, 8}},
    {"\"no\\\\ \\\"white\tspace\"", {JSS_IN_STRING, 13}},
    {"\"expect shortcuts \\\", \\\\, \\/, \b, \f, \n, \r, \t  \"", {JSS_IN_STRING, 30}},
    {"{\"foo\":\"foo\": 12}", {JSS_STRING_CLOSE, 12}},
    {"[1 true]", {JSS_WS_AFTER_VALUE_ARRAY, 3}},
    {"[\"x\"", {JSS_STRING_CLOSE, 4}},

};


static char dbg_output[0x2000];
static char* print_debug(struct json_tree * tree) {
    int j;
    int cursor = 0;
    struct json_token *stack = tree->stack;

    cs_memset(dbg_output, 0, sizeof dbg_output);
    for (j = 0; j < tree->token_count; ++j) {
        char dest[0x2000] = {0};

        cursor += snprintf(
            dbg_output + cursor, 80,
            "%d: kind: %s, root: %ld, len : %d, value: %s\n",
            j, (const char*[]){
                "JSON_UNSET", "JSON_TRUE", "JSON_FALSE", "JSON_NULL",
                "JSON_STRING", "JSON_NUMBER", "JSON_ARRAY", "JSON_OBJECT"
            }[stack[j].kind],
            stack[j].support_index,
            stack[j].length,
            (char*)cs_memcpy(dest, (char*)(stack[j].address), tok_len(&stack[j]))
        );
    }

    return dbg_output;
}

int test_parse_write() {
    puts("*** ALL SHOULD SUCCEED ***");

    long long frequency = start_profiler();
    long long total_parse_time = 0;
    long long total_write_time = 0;
    printf("Timer frequency: %lld by second.\n", frequency);

    char sink[CS_STATIC_POOL_SIZE] = {0};
    
    for (size_t i = 0; i < sizeof(valid_json) / sizeof(valid_json[0]) ; i++) {
        
        struct json_tree state = { 0 };
        printf("%zu: For >>> %s <<<\n", i, valid_json[i].str);
        
        long long start = start_timer();

        rjson(valid_json[i].str, &state);
        long long end = elapsed(start, frequency);
        total_parse_time += end;
        
        printf("Elapsed: %lld\n", end);
        printf("%s", print_debug(&state));
        
        start = start_timer();
        char* out = to_string(&state);
        end = elapsed(start, frequency);
        total_write_time += end;
        
        puts(out);
        puts(to_string_compact(&state));

        to_string_sink(&state, NULL, sink, sizeof sink);

        assert(state.cur_state == JSS_DOC_END);
        assert(state.cur_state != JSS_ERROR_STATE);
        assert(state.previous_state != JSS_ERROR_STATE);

        if(strcmp(valid_json[i].ref, "___") != 0) {
            printf("Len ref: %zu ; len ours: %zu\n", strlen(valid_json[i].ref), strlen((char*)sink));
            assert(strlen(sink) == strlen(valid_json[i].ref));
            assert(strcmp(to_string_compact(&state), valid_json[i].ref) == 0);
        }
    }

    puts("\n\n\n*** ALL SHOULD FAIL ***");

    for (size_t i = 0; i < sizeof(bogus_json) / sizeof(bogus_json[0]) ; i++) {

        struct json_tree state = { 0 };
        long long start = start_timer();

        rjson(bogus_json[i].str, &state);

        long long end = elapsed(start, frequency);
        total_parse_time += end;

        printf("%zu: For >>> %s <<<, elapsed: %lld\n", i, bogus_json[i].str, end);
        puts(state.parser_state.error_string);
        puts(print_debug(&state));

        start = start_timer();

        char* out = to_string(&state);
        end = elapsed(start, frequency);

        total_write_time += end;

        puts(out);

        assert(state.parser_state.cursor == bogus_json[i].dbg.cur + 1) ;
        assert(state.cur_state == JSS_ERROR_STATE);
        assert(state.previous_state == bogus_json[i].dbg.st);
    }
    
    // this test is to cover the regresion fixed by e88b3e9b
    {
        struct json_tree state = {0 };
        rjson("\"foo\"", &state);
        assert(state.token_count == 1);
        assert(state.stack[state.token_count - 1].kind == JSK_STRING);
    }
    
    puts("\n\n\n*** EX NIHILO ***");
    {
        struct json_tree state = { 0 };
        state.stack = static_stack;
        state.pool = static_pool;
        
        PUSH_ROOT(&state);
        START_AND_PUSH_TOKEN(&state, "[");
        PUSH_ROOT(&state);
        START_AND_PUSH_TOKEN(&state, "1");
        START_AND_PUSH_TOKEN(&state, "2");
        START_AND_PUSH_TOKEN(&state, "\"foo\"");
        START_AND_PUSH_TOKEN(&state, "[");
        PUSH_ROOT(&state);
        START_AND_PUSH_TOKEN(&state, "1");
        START_AND_PUSH_TOKEN(&state, "2");
        CLOSE_ROOT(&state);
        START_AND_PUSH_TOKEN(&state, "4");
        
        puts(to_string(&state));
        puts(to_string_compact(&state));
        
        assert(strcmp(to_string_compact(&state),"[1,2,\"foo\",[1,2],4]") == 0);
    }

    puts("\n\n*** SMART EX NIHILO ***");

    {
        struct json_tree state = {0 };

        push_token(&state, "[");
        push_token(&state, "1");
        push_token(&state, "2");
        push_token(&state, "\"foo\"");
        push_token(&state, "[");
        push_token(&state, "1");
        push_token(&state, "2");
        push_token(&state, ">");
        push_token(&state, "4");
        
        puts(to_string(&state));
        puts(to_string_compact(&state));
        
        assert(strcmp(to_string_compact(&state),"[1,2,\"foo\",[1,2],4]") == 0);
    }

    {
        struct json_tree state = {0 };

        push_token(&state, "{");
        push_token(&state, "\"foo\"");
        push_token(&state, ":");
        push_token(&state, "\"bar\"");
        push_token(&state, ">");
        push_token(&state, "\"array\"");
        push_token(&state, ":");
        push_token(&state, "[");
        push_token(&state, "1");
        push_token(&state, "2");
        push_token(&state, "4");
        push_token(&state, ">");
        push_token(&state, ">");
        push_token(&state, "\"question\"");
        push_token(&state, ":");
        push_token(&state, "true");
        
        puts(to_string(&state));
        puts(to_string_compact(&state));
        
        assert(strcmp(to_string_compact(&state),"{\"foo\":\"bar\",\"array\":[1,2,4],\"question\":true}") == 0);
    }

    puts("\n\n*** STREAM EX NIHILO ***");

    {
        struct json_tree state = { 0 };
        
        stream_tokens(
          &state, '~',
          (char *) &(char[]) {
            "{~\"foo\"~:~\"bar\"~>~\"array\"~:~[~1~2~4~>~>~\"question\"~:~true~>~\"\"~:~null"
              
          }
        );
        
        puts(to_string(&state));
        puts(to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"foo\":\"bar\",\"array\":[1,2,4],\"question\":true,\"\":null}")
               == 0);
        
        
        puts("\n\n*** POINTERS ***");
        printf("%s", print_debug(&state));
        puts(to_string_pointer(&state, query(&state, "/foo")));
        
        assert(strcmp(to_string_pointer(&state, query(&state, "/foo")), "\"bar\"") == 0);
        
        puts(to_string_pointer(&state, query(&state, "/array/2")));
        
        assert(strcmp(to_string_pointer(&state, query(&state, "/array/2")), "4") == 0);
        
        puts(to_string_pointer(&state, query(&state, "/")));
        
        assert(strcmp(to_string_pointer(&state, query(&state, "/")), "null") == 0);
        
        puts(to_string_pointer(&state, query(&state, "")));
        
        assert(strcmp(to_string_pointer(&state, query(&state, "")), "{\"foo\":\"bar\",\"array\":[1,2,4],\"question\":true,\"\":null}") == 0);
        
        assert(query(&state, "/in existent") == NULL);
        assert(query(&state, "/array/5") == NULL);
    }

    puts("\n\n*** EDITION ***");

    {
        struct json_tree state = {0 };

        rjson("{\"mon\":[],\"tue\":[]}",
              &state);
        
        puts(to_string_compact(&state));
        
        assert(state.token_count == 5);
        assert(strcmp(to_string_compact(&state), "{\"mon\":[],\"tue\":[]}") == 0);
        
        char buf[4];
        int j;
        for (j = 0; j < 4; j++) {
            sprintf(buf, "%d", j);
            insert_token(&state, buf, j % 2 ? query(&state, "/mon") : query(&state, "/tue"));
        }
        assert(state.token_count == 9);
        
        puts(to_string_compact(&state));
        
        // we need a writeable memory
        stream_into(&state, query(&state, "/mon"), '+', (char*) &(char[]) {"true+false"});
        
        puts(to_string_compact(&state));
        
        assert(state.token_count == 11);
        
        inject("[1, 2, 3]", &state, query(&state, "/tue"));
        
        puts(to_string_compact(&state));
        
        assert(state.token_count == 15);
        
        inject("\"fri\"", &state, query(&state, ""));
        
        puts((char *)to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"mon\":[1,3,true,false],\"tue\":[0,2,[1,2,3]],\"fri\":}"
                      ) == 0);
        
        move_token(&state, query(&state, "/mon"), query(&state, "/fri/<"));
        puts(print_debug(&state));
        
        
        puts((char *)to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"mon\":,\"tue\":[0,2,[1,2,3]],\"fri\":[1,3,true,false]}"
                      ) == 0);
        
        delete_token(&state, query(&state, "/mon/<"));
        
        puts((char *)to_string(&state));
        puts((char *)to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"tue\":[0,2,[1,2,3]],\"fri\":[1,3,true,false]}"
                      ) == 0);
        
        delete_token(&state, query(&state, "/tue/2"));
        
        puts((char *)to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"tue\":[0,2],\"fri\":[1,3,true,false]}"
                      ) == 0);
        
        delete_token(&state, query(&state, "/tue/<"));
        
        puts((char *)to_string_compact(&state));
        
        assert(strcmp(
                      to_string_compact(&state),
                      "{\"fri\":[1,3,true,false]}"
                      ) == 0);
    }
    
    puts("\n\n*** TWO TREES ***");
    {
        struct json_tree state = {0};
        struct json_tree state2 = {0};
        
        struct json_token stack1[40] = {0};
        struct json_token stack2[40] = {0};
        char pool1[400] = {0};
        char pool2[400] = {0};
        
        state.stack = stack1;
        state.pool = pool1;
        state2.stack = stack2;
        state2.pool = pool2;
        
        rjson("[1, 2, 3, [4, 5, 6], [7, 8, 9]", &state);
        rjson("{\"foo\": 1", &state2);
        
        puts(to_string_compact(&state));
        puts(to_string_compact(&state2));
        
        assert(state.current_support == 0);
        assert(state2.current_support == 1);
        
        push_token(&state2, ">");
        assert(state2.current_support == 0);
        insert_token(&state2, "\"array\"", query(&state2, ""));
        insert_token(&state2, ":", query(&state2, "/array/<"));
        
        assert(state2.current_support == 3);
        move_token(&state, query(&state, "/1"), query(&state, "/4"));
        
        puts(to_string_compact(&state));
        puts(to_string_compact(&state2));
        
        assert(strcmp(to_string_compact(&state), "[1,3,[4,5,6],[7,8,9,2]]") == 0);
        
        inject(
               to_string_pointer( &state, query(&state, "/3")),
               &state2,
               query(&state2, "/array/<"));
        
        delete_token(&state, query(&state, "/3"));
        
        rename_string(&state2, query(&state2, "/foo/<"), "bar");
        
        puts(to_string_compact(&state));
        puts(to_string_compact(&state2));
        
        assert(strcmp(to_string_compact(&state), "[1,3,[4,5,6]]") == 0);
        assert(strcmp(to_string_compact(&state2), "{\"bar\":1,\"array\":[7,8,9,2]}") == 0);
        
        char buff[30] = { 0 };
        int len = 0;
        struct json_token *root, *cur;
        for(root = query(&state2, "/array"),
            cur = next_child(&state2, root, NULL);
            cur;
            cur = next_child(&state2, root, cur)) {
            len += sprintf(buff+len, "%s", to_string_pointer(&state2, cur));
        }
        
        puts(buff);
        
        assert(strcmp(buff, "7892") == 0);
        
        cs_memset(buff, 0, sizeof buff);
        len = 0;
        for(root = query(&state2, ""),
            cur = next_child(&state2, root, NULL);
            cur;
            cur = next_child(&state2, root, cur)) {
            len += sprintf(buff+len, "%s", to_string_pointer(&state2, next_child(&state2, cur, NULL)));
        }
        
        puts(buff);
        
        assert(strcmp(buff, "1[7,8,9,2]") == 0);
        
        cs_memset(buff, 0, sizeof buff);
        len = 0;
        for(root = query(&state2, ""),
            cur = next_child(&state2, root, NULL);
            cur != NULL;
            cur = next_child(&state2, root, cur)) {
            len += sprintf(buff+len, "%s", to_string_pointer(&state2, cur));
        }
        
        puts(buff);
        
        assert(strcmp(buff, "\"bar\":1\"array\":[7,8,9,2]") == 0);
        
        cs_memset(buff, 0, sizeof buff);
        len = 0;
        for(root = query(&state2, ""),
            cur = next_child(&state2, root, NULL);
            cur;
            cur = next_child(&state2, root, cur)) {
            len += sprintf(
               buff+len, "%.*s",
               *(int*)((char*)cur->address - sizeof (int)),
               cur->address
           );
        }
        
        puts(buff);
        
        assert(len==12);
        assert(strcmp(buff, "\"bar\"\"array\"") == 0);
    }

    typedef struct json_tree t_tree;
    typedef struct json_token t_tok;
    // RECIPES
    {
        puts("*** RECIPES ***");

        struct json_tree tree = { 0 };
        rjson("[{\"tmp\": 1, \"tmp2\": 2}, {\"tmp\": 3, \"tmp2\": 4}, {\"tmp\": 5, \"tmp2\": 6}]",
            &tree
        );

#define nc(tree, host, cur) next_child(&tree, host, cur)
#define tsp(tree, tok) to_string_pointer(tree, tok)
        char res[20] = {0};
        int i = 0;
        t_tok * array = query(&tree, "");
        t_tok * obj = nc(tree, array, NULL);
        for (;obj != NULL; obj = nc(tree, array, obj), i++) {
            char buf1[20] = {0};
            char buf2[20] = {0};
            sprintf(buf1, "%d/tmp", i);
            sprintf(buf2, "%d/tmp2", i);
            strcat(res, tsp(&tree, query(&tree, buf1) )); // 1, 3, 5
            strcat(res, tsp(&tree, query(&tree, buf2) )); // 2, 4, 6
        }
        puts(res);
        fflush(stdout);
        assert(strcmp(res, "123456") == 0);


        struct json_tree json_tree = { 0 };
        rjson("[10, 20, 30, 40]", &json_tree);

        puts(to_string_compact(&json_tree)); // [10, 20, 30, 40]
        assert(strcmp(to_string_compact(&json_tree), "[10,20,30,40]") == 0);

        puts(to_string_sink(&json_tree, query(&json_tree, "/2"), sink, 0x100)); // 30
        assert(strcmp((char*)sink, "30") == 0);

        fflush(stdout);

        inject("{\"foo\": 12}", &json_tree, query(&json_tree, "/2"));

        puts(to_string(&json_tree)); // [10, 20, 30, 40]
        assert(strcmp(to_string_compact(&json_tree), "[10,20,30,40,{\"foo\":12}]") == 0);

        // [10, 20, 30, 40, {"foo": 12}]

        puts(to_string_pointer(&json_tree, query(&json_tree, "/4/foo"))); // 12
        puts(to_string_pointer(&json_tree, query(&json_tree, "/4/foo/<"))); // "foo":12
        puts(to_string_pointer(&json_tree, query(&json_tree, "/3"))); // 30
        puts(to_string_pointer(&json_tree, query(&json_tree, ""))); // [10, 20, 30, 40]
        fflush(stdout);
        assert(strcmp(to_string_pointer(&json_tree, query(&json_tree, "/4/foo")), "12") == 0);
        assert(strcmp(to_string_pointer(&json_tree, query(&json_tree, "/4/foo/<")), "\"foo\":12") == 0);
        assert(strcmp(to_string_pointer(&json_tree,query(&json_tree,"/3")), "40") == 0);
        assert(strcmp(to_string_pointer(&json_tree,query(&json_tree,"")), "[10,20,30,40,{\"foo\":12}]") == 0);

        struct json_token *root, *cur;
        for(root = query(&json_tree, ""),
                    cur = next_child(&json_tree, root, NULL);
            cur != NULL;
            cur = next_child(&json_tree, root, cur)) {
            printf(" %s", to_string_pointer(&json_tree, cur));
        }
        puts("");
    }

    // TO STRING POINTER FAIL
    {
#define tspf(tree, tok, fail) to_string_pointer_fail(tree, tok, fail)

        t_tree tree = { 0 };
        rjson("[{\"tmp\": 1, \"tmp2\": 2}, {\"tmp\": 3, \"tmp2\": 4}, {\"tmp2\": 6}]",
              &tree
        );

        char res[20] = {0};
        int i = 0;
        t_tok * array = query(&tree, "");
        t_tok * obj = nc(tree, array, NULL);
        for (;obj != NULL; obj = nc(tree, array, obj), i++) {
            char buf1[20] = {0};
            char buf2[20] = {0};
            sprintf(buf1, "%d/tmp", i);
            sprintf(buf2, "%d/tmp2", i);
            strcat(res, tspf(&tree, query(&tree, buf1), "null" )); // 1, 3, 5
            strcat(res, tspf(&tree, query(&tree, buf2), "null" )); // 2, 4, 6
        }
        puts(res);
        fflush(stdout);
        assert(strcmp(res, "1234null6") == 0);
#undef tspf
    }

    // WITHOUT LIBC
    {
#define tspf(tree, point, tok, fail) to_string_pointer_fail(tree, query_from(tree, point, tok), fail)

        t_tree tree = { 0 };
        rjson("[{\"tmp\": 1, \"tmp2\": 2}, {\"tmp\": 3, \"tmp2\": 4}, {\"tmp2\": 6}]",
              &tree
        );

        char res[20] = {0};
        int i = 0;
        t_tok * array = query(&tree, "");
        t_tok * obj = nc(tree, array, NULL);
        for (;obj != NULL; obj = nc(tree, array, obj), i++) {
            strcat(res, tspf(&tree, "/tmp", obj, "false" )); // 1, 3, 5
            strcat(res, tspf(&tree, "/tmp2", obj, "false" )); // 2, 4, 6
        }
        puts(res);
        fflush(stdout);
        assert(strcmp(res, "1234false6") == 0);
#undef tspf
    }


//    puts("\n\n*** NO COPY ***");
//    start_state_no_copy(&state, static_stack, NULL);
//    state.no_copy = 1;
//    const char* json = "[1, 2, 3, 4, 1234]";
//    enum json_errors error = rjson(json, &state);
//    puts(to_string_compact(&state));
//    assert(error == JSE_NO_ERRORS);
//    assert(query(&state, "/0")->address == (unsigned char*)json + 1);
//    assert(tok_len(query(&state, "/0")) == 1);
//    assert(tok_len(query(&state, "/4")) == 4);

    printf("Total parsing time: %lld\n", total_parse_time);
    printf("Total writing time: %lld\n", total_write_time);

#ifdef _MSC_VER
    FILE* file;
    int errorf = fopen_s(&file, "profile_data.txt", "a");
    if(errorf != 0) {
        char out[80] = { 0 };
        _strerror_s(out, 80, "err");
        puts(out);
    }
    fprintf(
            file,
            "Total parsing time: %lld ; Writing: %lld ; %s - %s\n",
            total_parse_time,
            total_write_time,
            LAST_COMMIT_COUNT, CMAKE_GENERATOR);
    fclose(file);
#endif


    return 0;
}

//int test_unit_

int main(int argc, char** argv) {
    
#ifdef TEST_SINGLE_HEADER
    puts("Single header tests.");
#endif
    
    if (argc > 1) {
        if (strcmp(argv[1], "--count-pass") == 0) {
            return sizeof valid_json / sizeof valid_json[0];
        }
        if (strcmp(argv[1], "--count-fail") == 0) {
            return sizeof bogus_json / sizeof bogus_json[0];
        }
    }
    
    // test that we can parse many kinds of json, catch grammar errors
    // and build json in various ways
    test_parse_write();
}
