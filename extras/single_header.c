#include "../cisson.h"

/**
 * When building a static or shared library that is made only
 * of an header file, toolchains on mac will ignore the header
 * file, as those are only useful for the preprocessor.
 *
 * This file is to be picked by toolchains so that the header-only
 * compilation unit gets built and passed to the linker.
 */
