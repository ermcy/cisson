function(tools_get_git_last_commit arg_git_dir arg_out_var)
    # returns the number of commits of a git repository
    # located at the <arg_git_dir> folder, and
    # puts it in the <arg_out_var> variable
    # in case of failure, returns 0


    find_package(Git QUIET)

    execute_process(
            COMMAND "${GIT_EXECUTABLE}" rev-list --count HEAD
            WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
            RESULT_VARIABLE _var_res
            OUTPUT_VARIABLE _var_LAST_COMMIT
            ERROR_QUIET
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if(_var_res)
        set(_var_LAST_COMMIT, "0")
    endif()
    unset(_var_res)

    set(${arg_out_var} "${_var_LAST_COMMIT}" PARENT_SCOPE)

endfunction()