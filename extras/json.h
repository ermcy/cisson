#ifndef JSON_JSON_H
#define JSON_JSON_H

#ifdef _MSC_VER
#define EXPORT(type, symbol) extern __declspec(dllexport) type symbol
#else
#define EXPORT(type, symbol) extern type symbol
#endif

#ifndef CS_STATIC_STACK_SIZE
#define CS_STATIC_STACK_SIZE 512
#endif

#ifndef CS_STATIC_POOL_SIZE
#define CS_STATIC_POOL_SIZE 0x20000
#endif

#ifdef WANT_LIBC
#include<stdio.h>
#include<string.h>
#include<stddef.h>
#include<stdbool.h>

#define cs_strlen(s) strlen(s)
#define cs_memset(dest, val, repeat) memset((dest), (val), (repeat))
#define cs_memcpy(dest, val, repeat) memcpy((dest), (val), (repeat))
#define cs_memcmp(v1, v2, size) memcmp((v1), (v2), (size))
#define cs_memmove(dest, src, size) memmove((dest), (src), (size))
#else

#ifndef NULL
#define NULL ((void *)0)
#endif

/* from MuslC */
static long long cisson_strlen(const char * s)
{
    const char *a = s;
    for (; *s; s++);
    return s-a;
}
#define cs_strlen(s) cisson_strlen((s))

static inline void *cisson_memset(void *dest, int c, long n)
{
    char *s = dest;

    for (; n; n--, s++) *s = (char )c;
    return dest;
}
#define cs_memset(dest, val, repeat) (cisson_memset((dest), (val), (repeat)))

static void * cisson_memcpy(void * dest, const void * src, long n)
{
    char *d = dest;
    const char *s = src;

    for (; n; n--) *d++ = *s++;
    return dest;
}
#define cs_memcpy(dest, val, repeat) (cisson_memcpy((dest), (val), (repeat)))

static inline int cisson_memcmp(const void *vl, const void *vr, long n)
{
    const char *l=vl, *r=vr;
    for (; n && *l == *r; n--, l++, r++);
    return n ? *l-*r : 0;
}
#define cs_memcmp(v1, v2, size) (cisson_memcmp((v1), (v2), (size)))

static inline void * cisson_memmove(void *dest, const void *src, long n)
{
    char *d = dest;
    const char *s = src;
    
    if(n == 0) return d;
    if (d==s) return d;
    if (s-d-n <= -2*n) return cisson_memcpy(d, s, n);

    if (d<s) {
        for (; n; n--) *d++ = *s++;
    } else {
        while (n) n--, d[n] = s[n];
    }

    return dest;
}
#define cs_memmove(dest, src, size) (cisson_memmove((dest), (src), (size)))
#endif  /* WANT_LIBC */

#define WHITESPACE \
  X(TABULATION, '\t') \
  X(LINE_FEED, '\n') \
  X(CARRIAGE_RETURN, '\r') \
  X(SPACE, ' ')

#define RAW_WHITESPACE "\t\n\r "
#define RAW_BOM_STARTER "\xEF"
#define RAW_BOM "\xEF\xBB\xBF"

#define X(a, b) a,
enum { WHITESPACE };
#undef X

#define RAW_DIGIT "123456789"

/**
 * Json structures are stored as a flat array of objects holding
 * an index to their parent in that array, index zero being the root.
 * Json doesn't require json arrays elements to have an order so 
 * sibling data is not stored, but the ordering can be known through
 * their index in that array.
 */
struct json_tree {
    enum json_state {
        JSS_ERROR_STATE = 0,
        JSS_DOC_START,
        JSS_DOC_END,
        JSS_BOM_2,
        JSS_BOM_3,
        JSS_TRUE_T,
        JSS_TRUE_R,
        JSS_TRUE_U,
        JSS_TRUE_E,
        JSS_FALSE_F,
        JSS_FALSE_A,
        JSS_FALSE_L,
        JSS_FALSE_S,
        JSS_FALSE_E,
        JSS_NULL_N,
        JSS_NULL_U,
        JSS_NULL_L,
        JSS_NULL_LL,
        JSS_WS_BEFORE_VALUE,
        JSS_WS_AFTER_VALUE_ROOT,
        JSS_WS_AFTER_VALUE_ARRAY,
        JSS_NUMBER_MINUS,
        JSS_NUMBER_ZERO,
        JSS_NUMBER_19,
        JSS_NUMBER_MINUS_ZERO,
        JSS_NUMBER,
        JSS_NUMBER_COMMA,
        JSS_NUMBER_COMMA_NUMBER,
        JSS_NUMBER_EXPO_e,
        JSS_NUMBER_EXPO_E,
        JSS_NUMBER_EXPO_MINUS,
        JSS_NUMBER_EXPO_PLUS,
        JSS_NUMBER_EXPO_NUMBER,
        JSS_STRING_START,
        JSS_STRING_CLOSE,
        JSS_STRING_ESCAPE_START,
        JSS_IN_STRING,
        JSS_ESCAPE_UNICODE_1,
        JSS_ESCAPE_UNICODE_2,
        JSS_ESCAPE_UNICODE_3,
        JSS_ESCAPE_UNICODE_4,
        JSS_ESCAPE_UNICODE_LAST,
        JSS_ARRAY_START,
        JSS_ARRAY_WS_EXPECT_VALUE,
        JSS_ARRAY_COMMA,
        JSS_OBJECT_START,
        JSS_OBJECT_COMMA,
        JSS_OBJECT_WS_AFTER_OPEN,
        JSS_OBJECT_COLON,
        JSS_OBJECT_WS_AFTER_COMMA,
        JSS_ARRAY_CLOSE,
        JSS_OBJECT_CLOSE,
        JSS_WS_AFTER_VALUE_OBJECT_KEY,
        JSS_WS_AFTER_VALUE_OBJECT_VALUE,
    } cur_state, previous_state;
    struct json_token {
        enum json_token_kind {
            JSK_UNSET = 0,
            JSK_TRUE,
            JSK_FALSE,
            JSK_NULL,
            JSK_STRING,
            JSK_NUMBER,
            JSK_ARRAY,
            JSK_OBJECT,
        } kind;
        long support_index;
        char * address;
        int length;
    } * stack;
    long token_count;
    char *pool;
    long pool_cursor;
    long current_support;
    struct {
        const char* string;
        long len;
        long cursor;
        char error_string[50];
    } parser_state;
    int (*parser)(struct json_tree *tree);
    int (*parser_processor)(struct json_tree *tree);
    int (*parser_error_handler)(struct json_tree *tree);
    int (*parser_bound_checker)(struct json_tree *tree);
};

static struct json_token static_stack[ CS_STATIC_STACK_SIZE ];
static char static_pool[ CS_STATIC_POOL_SIZE ];

/* Parsing */
EXPORT(void, rjson_)(
        struct json_tree * state
);
EXPORT(void, inject_)(
        struct json_tree * state,
        struct json_token * where
);

/* Dom Crawling */
EXPORT(struct json_token*, query_from_)(
        struct json_tree * state,
        long length,
        char *query,
        struct json_token * host
        );
EXPORT(struct json_token* , next_child_)(
        struct json_tree * tree,
        struct json_token * root,
        struct json_token * current);
#define next_child(tokens_, root_, current_)  next_child_((tokens_), (root_), (current_))

/* Output */

EXPORT(char* , to_string_)(
        struct json_tree * tree,
        struct json_token * start_token,
        int indent,
        char* sink,
        int sink_size);
/* Building */
EXPORT(void, start_string)(long *, char []);
EXPORT(void, push_string)(const long * cursor, char * pool, const char* string, long length);
EXPORT(void, close_root)(struct json_token *, long * );
EXPORT(void, push_root)(long * , const long * );
EXPORT(void, push_token_kind_)(
        char * address, struct json_tree *tree, long support_index, int length);
EXPORT(void, delete_token_)(struct json_tree* tree, struct json_token* which);
EXPORT(void, move_token_)(struct json_tree* state, struct json_token* which, struct json_token* where);
EXPORT(void, rename_string_)(struct json_tree* state, struct json_token* which, long len, const char* new_name);
/* EZ JSON */
EXPORT(void, insert_token_)(struct json_tree * state, char *token, struct json_token* root);
EXPORT(void, stream_tokens_)(struct json_tree * state, struct json_token * where, char separator, char stream[], long length);

#define tok_len(token) ((token)->length)

#define ROOT_TOKEN (-1)

#define rjson(text_, state_) rjson_len(cs_strlen((text_)) + 1, (text_), (state_))

#define rjson_len(len_, text_, state_) ( \
    ((state_)->parser_state.string = (text_)), \
    ((state_)->parser_state.len = (len_)), \
    ((state_)->parser_state.cursor = 0), \
    rjson_((state_))         \
)

#define inject(text_, state_, where_) (\
    ((state_)->parser_state.string = (text_)), \
    ((state_)->parser_state.len = cs_strlen((text_))), \
    ((state_)->parser_state.cursor = 0), \
    inject_((state_), (where_)) \
)

#define to_string(state_) (char * )to_string_( \
    (state_), (state_)->stack, 2, NULL, -1 \
)
#define to_string_compact(state_) (char * )to_string_( \
    (state_), (state_)->stack, 0, NULL, -1 \
)
#define to_string_pointer(state_, start) (char * )to_string_( \
    (state_), start, 0, NULL, -1 \
)
#define to_string_pointer_fail(state_, start, fail) ( \
    start \
    ? (char * )to_string_( \
        (state_), start, 0, NULL, -1) \
    : fail \
)
#define to_string_sink(state_, start, sink, size)to_string_( \
    (state_), start, 0, sink, size \
)

#define delete_token(tokens, which) delete_token_(tokens, which)
#define move_token(state, which, where) move_token_(state, which, where)
#define rename_string(state, which, new_name) rename_string_(state, which, cs_strlen((new_name)), new_name)
#define insert_token(state, token, root) insert_token_((state), (token), root)
#define push_token(state_, token_)  \
    ( \
    ((state_)->stack == 0 \
        ? ( \
            (state_)->stack = static_stack, \
            (state_)->pool = static_pool, \
            (state_)->current_support = -1 \
        ) \
        : 0 \
    ) \
        , insert_token_((state_), (token_), &(state_)->stack[(state_)->current_support]) \
    )

#define query(state, string) query_from(state, string, (state)->stack)
#define query_from(state, string, from) query_from_((state), cs_strlen((string)), (string), from)
#define stream_tokens(state, sep, stream) stream_tokens_( \
    (state),                                              \
    &(state)->stack[(state)->current_support],     \
    (sep), (stream), cs_strlen((stream)))
#define stream_into(state, where, sep, stream) \
    stream_tokens_((state), (where), (sep), (stream), cs_strlen((stream)))

#define START_STRING(state_) \
    start_string(&(state_)->pool_cursor, (state_)->pool)

#define PUSH_STRING(state_, string_, length_) \
    push_string(                             \
        &(state_)->pool_cursor,             \
        (state_)->pool,                \
        (string_),                             \
        (length_))

#define CLOSE_ROOT(state_) close_root \
    ((*(state_)).stack, &(*(state_)).current_support)

#define PUSH_ROOT(state_) push_root \
    (&(state_)->current_support, &(state_)->token_count)

#define INSERT_TOKEN_LEN(address_, state_, root_, len_) \
    push_token_kind_(   \
        (address_),                 \
        state_,          \
        (root_),                    \
        (len_))

#define PUSH_TOKEN_LEN(address_, len_, state_) \
    INSERT_TOKEN_LEN((address_), (state_), (state_)->current_support, (len_))

#define INSERT_TOKEN(address_, state_, root_) \
    INSERT_TOKEN_LEN(((address_) + sizeof (int)), (state_), (root_), *(int*)(void*)((address_)) )

#define PUSH_STRING_TOKEN(state_) \
    INSERT_STRING_TOKEN((state_), (state_)->current_support)

#define INSERT_STRING_TOKEN(state_, root) \
    INSERT_TOKEN(                                \
        (state_)->pool + (state_)->pool_cursor,  \
        (state_),                                \
        (root)                                   \
    )

#define START_AND_PUSH_TOKEN(state_, string_) \
    START_AND_INSERT_TOKEN((state_), (string_), (state_)->current_support)

#define START_AND_INSERT_TOKEN(state_, string_, root_) \
    do { \
        START_STRING(state_);               \
        PUSH_STRING((state_), (string_), (cs_strlen ((string_)))); \
        INSERT_STRING_TOKEN((state_), (root_)); \
    }while(0)

#endif //JSON_JSON_H
