#!cmake -P

message(STATUS "Generating single header file")

file(READ extras/json.h _var_header)

string(LENGTH "#include \"json.h\"\n" to_cut)
file(READ extras/json.c _var_body OFFSET ${to_cut})

file(WRITE cisson.h "// Script Generated file, DO NOT EDIT!")

file(APPEND cisson.h "\n${_var_header}")
file(APPEND cisson.h "#ifdef JSN_IMPLEMENTATION\n")
file(APPEND cisson.h "${_var_body}")
file(APPEND cisson.h "\n#endif  // JSN_IMPLEMENTATION\n")

message(STATUS "Done. ~> cisson.h")
